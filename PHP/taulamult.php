<!DOCTYPE html>
<html>
<head>
   <title>Tabla de multiplicar</title>
   <meta name="author" content="Andrés Quiròs">
</head>
<body>
<h1>Tabla de multiplicar</h1>
<h3>Escribe un número</h3>
<form action="#" method="post">
   <p>Escribe aquí el número: <input type="text" name="num" maxlength="2" size="2" /></p>
   <p><input type="submit" value="Ver tabla de multiplicar." /></p>
</form>
<?php  
$n=$_POST['num'];
    echo "<h4>Tabla del $n:</h4>";
    $i=1;
    while ($i<=10) {
        echo "$n x $i = ".$n*$i."<br/>";
        $i++;
    } 
?>
</body>
</html>