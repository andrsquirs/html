<!DOCTYPE html>
<html>
<head>
   <title>Tabla de multiplicar</title>
   <link rel="STYLESHEET" type="text/css" href="PHP1_1_QuirosAndres.css">
</head>
<body>
<h1>Tabla de multiplicar</h1>
<h3>Escribe un número</h3>
<form action="#" method="post">
   <p>Escribe aquí el número: <input type="text" name="num" maxlength="2" size="2" /></p>
   <p><input type="submit" value="Ver tabla de multiplicar." /></p>
</form>
<table>
<?php  
$n=$_POST['num'];
	echo "<tr>";
    echo "<th>" . "<h4>Tabla del $n:</h4>" . "</th>";
    $i=1;
    echo "</tr>";
    while ($i<=10) {
		echo "<tr>";
        echo "<td>$n x $i = ".$n*$i . "</td>";
        echo "</tr>";
        $i++;
    } 
    
?>
</table>
<footer>
	<p>Fet per Andrés Quirós</p>
</footer>
</body>
</html>
